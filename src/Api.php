<?php namespace WAI;

/**
 * Class Api
 * @package WAI
 */
class Api {
    /**
     * @var string
     */
    private $url;
    /**
     * @var
     */
    private $client;
    /**
     * @var
     */
    private $secret;
    /**
     * @var array
     */
    private $options;
    /**
     * @var array
     */
    private $constants = [];

    /**
     * @var string
     */
    public $algorithm = 'sha256';

    /**
     * Api constructor.
     * @param $url
     * @param $client
     * @param $secret
     * @param array $options
     */
    public function __construct($url, $client, $secret, array $options = []) {
        $options = array_merge([
            'debug' => true,
            'load_constants' => false,
            'timeout' => 300
        ], $options);
        $this->url = rtrim($url, '/');
        $this->client = $client;
        $this->secret = $secret;
        $this->options = $options;
        if ($options['load_constants']) {
            $fromCache = \Cache::get('getConstants-'.$this->client);
            if(is_null($fromCache)) {
                $response = $this->call('_get_constants', [], true);
                if (!$response['success']) {
                    throw new Exception($response['exception']['message'], $response['exception']['code']);
                }
                $fromCache = $response['result'];
                \Cache::put('getConstants-'.$this->client, $fromCache, \Carbon\Carbon::now()->addHours(24));
            }
            $this->constants = $fromCache;
        }
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments) {
        return $this->call($name, $arguments);
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name) {
        if (!isset($this->constants[$name])) {
            return null;
        }
        return $this->constants[$name];
    }

    /**
     * @param $path
     * @param array $data
     * @param null $debug
     * @return mixed
     */
    public function call($path, $data = [], $debug = null) {
        if ($debug === null) {
            $debug = $this->options['debug'];
        }
        $call = [
            'path' => ltrim($path, '/'),
            'data' => $data,
            'timeout' => $this->options['timeout']
        ];
        $response = $this->_call($call);
        if ($debug) {
            return $response;
        }
        if ($response['success']) {
            return $response['result'];
        } else {
            throw new Exception($response['exception']['message'], $response['exception']['code']);
        }
    }

    /**
     * @param $data
     * @param bool $salt
     * @return string
     */
    private function _hmac($data, $salt = false) {
        if ($salt === true) {
            $salt = dechex(mt_rand());
        }
        if ($salt !== false) {
            $data .= '.' . $salt;
        }
        $hash = hash_hmac($this->algorithm, $data, $this->secret);
        if ($salt !== false) {
            return $hash . '.' . $salt;
        } else {
            return $hash;
        }
    }

    /**
     * @param array $call
     * @return mixed
     */
    private function _call(array $call) {
        if (is_array($call['data'])) {
            $call['data'] = json_encode($call['data']);
        }
        $hash = $this->_hmac(serialize(['path' => $call['path'], 'data' => $call['data']]), true);
        $auth = 'Simply ' . $this->client . ':' . base64_encode($hash);
        $url = $this->url . '/' . $call['path'];
        $netstart = microtime(true);
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($c, CURLOPT_POSTFIELDS, $call['data']);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HTTPHEADER, ['X-Simply-Auth: ' . $auth]);
        $res = curl_exec($c);
        if ($res === false) {
            $error = curl_error($c);
            $errno = curl_errno($c);
            curl_close($c);
            throw new Exception('cURL error: ' . $error, $errno);
        }
        curl_close($c);
        $response = json_decode($res, true);
        if ($response === null) {
            throw new Exception('Invalid response "' . $res . '".');
        }
        $response['time']['net'] = microtime(true) - $netstart;
        return $response;
    }
}
