<?php

namespace WAI\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendContactFormEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $timeout = 30;

    protected $form;
    protected $to;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($form, $to)
    {
        $this->form = $form;
        $this->to = $to;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $view = 'waitools::emails.contact';
        if(\View::exists('emails.contact')) {
            $view = 'emails.contact';
        }

        $form = $this->form;
        $to = $this->to;

        \Mail::send($view, ['form'=>$form], function ($m) use ($form, $to) {
            $m->to($to['email'], $to['name']);
            $m->replyTo($form['email']);
            $m->subject($to['name'].' - Web Enquiry');
        });
    }
}
