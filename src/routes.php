<?php

// svg
Route::get('/svg/{svgName}/{colors}.svg', ['uses' => '\WAI\Svg@controllerSvg', 'as'=>'svgConverterSvg']);
Route::get('/svg/{svgName}/{colors}.jpeg', ['uses' => '\WAI\Svg@controllerJpeg', 'as'=>'svgConverterJpeg']);

// tools
Route::get('/cc', ['uses' => '\WAI\Tools@clearCache', 'as'=>'clearCache']);

// sitemap
Route::get('/sitemap', ['uses' => '\WAI\Sitemap@getSitemap', 'as'=>'sitemap']);
Route::get('/sitemap.xml', ['uses' => '\WAI\Sitemap@getSitemap', 'as'=>'sitemap.xml']);

// form
Route::post('/form/contact', ['uses' => '\WAI\Form@submitForm', 'as'=>'contactFormSubmit']);


