<?php namespace WAI;

/**
 * Class Emailer
 * @package WAI
 */
class Emailer
{
    private static $apiUrl = null;
    private static $apiKey = null;
    private static $apiSecret = null;

    public static function getConfig()
    {
        self::$apiUrl = config('waitools.emailer.url', 'http://emailer.contactmedia.co.za/api/0.1/');
        self::$apiKey = config('waitools.emailer.key', 'capemediacorporation');
        self::$apiSecret = config('waitools.emailer.secret', 'nsa2nvsl34');
    }

    public static function doApiCall($endPoint='', $params=[])
    {
        $url = self::$apiUrl.$endPoint.'?apikey='.self::$apiKey.'&secret='.self::$apiSecret;
        foreach($params as $key=>$value) {
            $url.='&'.$key.'='.$value;
        }
        $response = json_decode(file_get_contents($url), true);

        if(empty($response['response'])) {
            return null;
        } else {
            return $response['response'];
        }
    }

    public static function getSubscriberInfo($subscriberId)
    {
        if(strlen($subscriberId)<10) {
            $response = self::doApiCall('email', ['key'=>$subscriberId]);
            if($response) {
                return $response['subscriber'];
            }
        } else {
            $subscriber=base64_decode($subscriberId);
            if($subscriber) {
                return ['email'=>$subscriber];
            }
        }
    }

    public static function getList($listId)
    {
        return self::doApiCall('list', ['id'=>$listId]);
    }
}

Emailer::getConfig();