<?php namespace WAI;

use Illuminate\Support\ServiceProvider;

class WAIToolsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setupConfig();
        $this->loadViewsFrom(__DIR__ . '/../../../roumen/sitemap/src/views', 'sitemap');
        $this->loadViewsFrom(__DIR__ . '/views', 'waitools');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        require_once(__DIR__.'/globals.php');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Setup the config.
     *
     * @return void
     */
    protected function setupConfig()
    {
        $source = realpath(__DIR__.'/../config/waitools.php');

        if ($this->app instanceof \Illuminate\Foundation\Application && $this->app->runningInConsole()) {
            $this->publishes([$source => config_path('waitools.php')], 'config');
            $this->publishes([__DIR__.'/../folders/cover' => public_path('cover')], 'public');
            $this->publishes([__DIR__.'/../folders/issues' => storage_path('issues')], 'storage');
            $this->publishes([__DIR__.'/../folders/svg' => storage_path('svg')], 'svg');
            $this->publishes([__DIR__.'/../folders/svg-public' => public_path('svg')], 'svg');

            if(file_exists(public_path('cover'))) {
                chmod(public_path('cover'), 0777);
            }

            if(file_exists(public_path('svg'))) {
                chmod(public_path('svg'), 0777);
            }

            $this->mergeConfigFrom($source, 'waitools');
        } else {
            return;
        }
    }

}
